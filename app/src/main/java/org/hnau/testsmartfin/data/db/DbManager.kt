package org.hnau.testsmartfin.data.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import org.hnau.testsmartfin.data.entity.Product


object DbManager {

    private const val DB_NAME = "data"

    fun openDB(
        context: Context
    ): SQLiteDatabase = object : SQLiteOpenHelper(
        context,
        DB_NAME,
        null,
        1
    ) {

        override fun onCreate(db: SQLiteDatabase) {
            Product.createDbTable(db)
        }

        override fun onUpgrade(
            db: SQLiteDatabase,
            oldVersion: Int,
            newVersion: Int
        ) {
        }

    }.readableDatabase

}