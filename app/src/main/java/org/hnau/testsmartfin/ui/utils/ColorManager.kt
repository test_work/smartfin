package org.hnau.testsmartfin.ui.utils

import android.content.Context
import org.hnau.testsmartfin.R
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.dp_px.dp2
import ru.hnau.androidutils.context_getters.dp_px.dp4
import ru.hnau.androidutils.context_getters.dp_px.dp8
import ru.hnau.androidutils.ui.drawer.ripple.info.RippleDrawInfo
import ru.hnau.androidutils.ui.drawer.ripple.info.RippleInfo
import ru.hnau.androidutils.ui.drawer.shadow.info.ButtonShadowInfo
import ru.hnau.androidutils.ui.drawer.shadow.info.ShadowInfo
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.material.MaterialDialogViewInfo
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.material.button.MaterialDialogButtonInfo
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.material.title.MaterialDialogTitleInfo
import ru.hnau.androidutils.ui.view.waiter.material.MaterialWaiterView
import ru.hnau.androidutils.ui.view.waiter.material.drawer.params.MaterialWaiterColor
import ru.hnau.androidutils.ui.view.waiter.material.drawer.params.MaterialWaiterSize
import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.producer.locked_producer.LockedProducer


object ColorManager {

    val TRANSPARENT = ColorGetter.TRANSPARENT
    val BLACK = ColorGetter.BLACK
    val WHITE = ColorGetter.WHITE

    val PRIMARY = ColorGetter.byResId(R.color.primary)
    val ADDITIONAL = ColorGetter.byResId(R.color.additional)
    val BG = ColorGetter.byResId(R.color.bg)
    val FG = ColorGetter.byResId(R.color.fg)

    val PRIMARY_T75 = PRIMARY.mapWithAlpha(0.75f)

    val RIPPLE_INFO = RippleInfo()

    val DRAW_RIPPLE_ALPHA = 0.25f

    val FG_ON_BG_RIPPLE_DRAW_INFO = RippleDrawInfo(
        backgroundColor = BG,
        color = FG,
        rippleInfo = RIPPLE_INFO,
        rippleAlpha = DRAW_RIPPLE_ALPHA
    )

    val PRIMARY_ON_TRANSPARENT_RIPPLE_DRAW_INFO = RippleDrawInfo(
        backgroundColor = TRANSPARENT,
        color = PRIMARY,
        rippleInfo = RIPPLE_INFO,
        rippleAlpha = DRAW_RIPPLE_ALPHA
    )

    val BG_ON_TRANSPARENT_RIPPLE_DRAW_INFO = RippleDrawInfo(
        color = BG,
        backgroundColor = TRANSPARENT,
        rippleInfo = RIPPLE_INFO,
        rippleAlpha = DRAW_RIPPLE_ALPHA
    )

    val DEFAULT_SHADOW_INFO = ShadowInfo(
        dp4, dp8,
        BLACK, 0.25f
    )
    val DEFAULT_PRESSED_SHADOW_INFO = ShadowInfo(
        dp2, dp4,
        BLACK, 0.25f
    )
    val DEFAULT_BUTTON_SHADOW_INFO = ButtonShadowInfo(
        normal = DEFAULT_SHADOW_INFO,
        pressed = DEFAULT_PRESSED_SHADOW_INFO,
        animationTime = TimeValue.MILLISECOND * 100
    )

    fun createWaiterView(
        context: Context,
        lockedProducer: LockedProducer,
        size: MaterialWaiterSize = MaterialWaiterSize.LARGE
    ) =
        MaterialWaiterView(
            context = context,
            lockedProducer = lockedProducer,
            size = size,
            color = MaterialWaiterColor(
                foreground = PRIMARY,
                background = WHITE.mapWithAlpha(0.5f)
            ),
            visibilitySwitchingTime = TimeValue.MILLISECOND * 500
        )

    val MATERIAL_DIALOG_VIEW_INFO = MaterialDialogViewInfo(
        button = MaterialDialogButtonInfo.DEFAULT.copy(
            textColor = PRIMARY,
            rippleDrawInfo = PRIMARY_ON_TRANSPARENT_RIPPLE_DRAW_INFO
        ),
        title = MaterialDialogTitleInfo.DEFAULT.copy(
            labelInfo = MaterialDialogTitleInfo.DEFAULT.labelInfo.copy(
                textColor = PRIMARY
            )
        )
    )


}