package org.hnau.testsmartfin.ui.app_activity.content.count_selector.input.keyboard.button

import android.content.Context
import android.graphics.Canvas
import android.view.MotionEvent
import android.view.View
import org.hnau.testsmartfin.ui.utils.ColorManager
import ru.hnau.androidutils.animations.smooth
import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.context_getters.dp_px.dp2
import ru.hnau.androidutils.context_getters.dp_px.dp64
import ru.hnau.androidutils.ui.bounds_producer.createBoundsProducer
import ru.hnau.androidutils.ui.canvas_shape.CircleCanvasShape
import ru.hnau.androidutils.ui.drawables.layout_drawable.LayoutDrawable
import ru.hnau.androidutils.ui.drawer.border.BorderDrawer
import ru.hnau.androidutils.ui.drawer.border.BorderInfo
import ru.hnau.androidutils.ui.drawer.ripple.RippleDrawer
import ru.hnau.androidutils.ui.view.utils.*
import ru.hnau.androidutils.ui.view.utils.touch.TouchHandler
import ru.hnau.jutils.getFloatInterFloats
import ru.hnau.jutils.ifFalse
import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.producer.extensions.observeWhen
import ru.hnau.jutils.producer.extensions.toFloat


open class CountSelectorButton(
    context: Context,
    icon: DrawableGetter,
    value: Producer<String>,
    valueIsCorrectChecker: (String) -> Boolean,
    valueOnClickConverter: (String) -> String,
    private val valueIsChanged: (String) -> Unit
) : View(
    context
) {

    companion object {

        private val PREFERRED_SIZE = dp64

    }

    private val icon = LayoutDrawable(
        context = context,
        initialContent = icon
    )

    private val borderInfo = BorderInfo(
        width = dp2,
        color = ColorManager.PRIMARY
    )

    private val boundsProducer =
        createBoundsProducer().applyInsents(context, borderInfo.insets)

    private val canvasShape = CircleCanvasShape(
        boundsProducer = boundsProducer
    )

    private val touchHandler = TouchHandler(
        canvasShape = canvasShape
    ) {
        valueIsChanged(nextValue)
    }

    private val isVisibleToUserProducer =
        createIsVisibleToUserProducer()

    private val rippleDrawer = RippleDrawer(
        animatingView = this,
        animatingViewIsVisibleToUser = isVisibleToUserProducer,
        rippleDrawInfo = ColorManager.PRIMARY_ON_TRANSPARENT_RIPPLE_DRAW_INFO,
        touchHandler = touchHandler,
        canvasShape = canvasShape
    )

    private val borderDrawer = BorderDrawer(
        canvasShape = canvasShape,
        context = context,
        borderInfo = borderInfo
    )

    private val nextValueProducer =
        value
            .observeWhen(isVisibleToUserProducer)
            .map(valueOnClickConverter)

    private val nextValueIsCorrectProducer =
        nextValueProducer
            .map(valueIsCorrectChecker)

    private var nextValueIsCorrect = false

    private var nextValue = ""

    init {

        nextValueProducer
            .attach { nextValue = it }

        nextValueIsCorrectProducer
            .attach { nextValueIsCorrect = it }

        nextValueIsCorrectProducer
            .toFloat()
            .smooth()
            .attach { visibility ->
                alpha = getFloatInterFloats(0.25f, 1f, visibility)
            }

        boundsProducer.attach { bounds ->
            this@CountSelectorButton.icon.setBounds(
                bounds.left.toInt(),
                bounds.top.toInt(),
                bounds.right.toInt(),
                bounds.bottom.toInt()
            )
        }
    }

    override fun draw(canvas: Canvas) {
        super.draw(canvas)
        rippleDrawer.draw(canvas)
        borderDrawer.draw(canvas)
        icon.draw(canvas)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        super.onTouchEvent(event)
        nextValueIsCorrect.ifFalse { return false }
        touchHandler.handle(event)
        return true
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {

        val width = getDefaultMeasurement(
            widthMeasureSpec,
            PREFERRED_SIZE.getPxInt(context) + horizontalPaddingSum
        )

        val height = getDefaultMeasurement(
            heightMeasureSpec,
            PREFERRED_SIZE.getPxInt(context) + verticalPaddingSum
        )

        super.onMeasure(
            makeExactlyMeasureSpec(width),
            makeExactlyMeasureSpec(height)
        )
    }

}