package org.hnau.testsmartfin.utils

import org.hnau.testsmartfin.R
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.jutils.handle


inline class Amount(
    val kops: Long
) {

    companion object {

        val ZERO = Amount(0)

        private const val KOPS_IN_RUBLE = 100

        private val RUB_SIGN = StringGetter(R.string.ruble)

        fun byRubles(rubles: Long) =
            Amount(rubles * KOPS_IN_RUBLE)

    }

    fun toUiString(forceKops: Boolean = false) =
        StringGetter { context ->

            val rubs = (kops / KOPS_IN_RUBLE).toString()

            val kops = kops.rem(KOPS_IN_RUBLE)
                .takeIf { forceKops || it > 0 }
                ?.let { kops ->
                    "," + (kops < 10).handle(
                        forTrue = "0$kops",
                        forFalse = kops.toString()
                    )
                }
                ?: ""

            return@StringGetter "$rubs$kops ${RUB_SIGN.get(context)}"
        }

    operator fun plus(other: Amount) =
        Amount(kops + other.kops)

    operator fun times(count: Int) =
        Amount(kops * count)

    operator fun times(count: Long) =
        Amount(kops * count)

}