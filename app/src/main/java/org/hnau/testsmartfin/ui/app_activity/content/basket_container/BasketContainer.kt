package org.hnau.testsmartfin.ui.app_activity.content.basket_container

import android.content.Context
import android.widget.LinearLayout
import org.hnau.testsmartfin.data.entity.BasketItem
import org.hnau.testsmartfin.ui.app_activity.content.basket_container.bottom.BasketContainerBottomView
import org.hnau.testsmartfin.ui.app_activity.content.basket_container.top.BasketInfoContainer
import org.hnau.testsmartfin.ui.utils.ColorManager
import ru.hnau.androidutils.ui.utils.ScreenManager
import ru.hnau.androidutils.ui.view.LinearLayoutSeparator
import ru.hnau.androidutils.ui.view.utils.apply.addChild
import ru.hnau.androidutils.ui.view.utils.apply.applyBackground
import ru.hnau.androidutils.ui.view.utils.apply.applyVerticalOrientation
import ru.hnau.androidutils.ui.view.utils.apply.layout_params.applyLinearParams
import ru.hnau.jutils.producer.ActualProducerSimple


class BasketContainer(
    context: Context
) : LinearLayout(
    context
) {

    private val basketItems = ActualProducerSimple<List<BasketItem>>(emptyList())

    private val itemsView = BasketInfoContainer.create(
        context = context,
        items = basketItems,
        onClearItemsButtonClick = this::clearBasketItems
    )
        .applyLinearParams {
            setStretchedHeight()
            setMatchParentWidth()
        }

    init {
        applyVerticalOrientation()
        applyBackground(ColorManager.BG)
        addChild(
            LinearLayoutSeparator(
                context = context,
                backgroundColor = ColorManager.PRIMARY_T75
            ).applyLinearParams {
                setMatchParentWidth()
                setHeight(ScreenManager.statusBarHeight)
            }
        )
        addChild(itemsView)
        addChild(
            BasketContainerBottomView(
                context = context,
                items = basketItems,
                onPay = this::clearBasketItems
            )
        )
    }


    fun onBasketItemAdded(basketItem: BasketItem) =
        basketItems.updateState(basketItems.currentState + basketItem)

    fun clearBasketItems() =
        basketItems.updateState(emptyList())

}