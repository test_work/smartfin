package org.hnau.testsmartfin.utils

import org.hnau.testsmartfin.R
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.jutils.handle


inline class Weight(
    val gramms: Long
) {

    companion object {

        val EMPTY = Weight(0)

        const val KG_IN_G = 1000

        private val KG_SIGN = StringGetter(R.string.kg)

        fun byKGs(kg: Long) =
            Weight(kg * KG_IN_G)

    }

    fun toUiString(forceGramms: Boolean = false) =
        StringGetter { context ->

            val kg = (gramms / KG_IN_G).toString()

            val g = gramms.rem(KG_IN_G)
                .takeIf { forceGramms || it > 0 }
                ?.let { g ->
                    "." + when {
                        g < 10 -> "00$g"
                        g < 100 -> "0$g"
                        else -> g.toString()
                    }
                }
                ?: ""

            return@StringGetter "$kg$g${KG_SIGN.get(context)}"
        }

    operator fun plus(other: Weight) =
        Weight(gramms + other.gramms)

    operator fun times(count: Float) =
        Weight((gramms * count).toLong())

}