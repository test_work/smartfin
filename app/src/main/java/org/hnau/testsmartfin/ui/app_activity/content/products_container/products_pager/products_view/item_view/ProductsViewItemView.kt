package org.hnau.testsmartfin.ui.app_activity.content.products_container.products_pager.products_view.item_view

import android.content.Context
import android.graphics.Canvas
import android.view.MotionEvent
import android.widget.FrameLayout
import android.widget.LinearLayout
import org.hnau.testsmartfin.data.entity.Product
import org.hnau.testsmartfin.ui.utils.ColorManager
import org.hnau.testsmartfin.ui.utils.SizeManager
import ru.hnau.androidutils.context_getters.dp_px.*
import ru.hnau.androidutils.context_getters.toGetter
import ru.hnau.androidutils.ui.bounds_producer.createBoundsProducer
import ru.hnau.androidutils.ui.canvas_shape.RoundCornersRectCanvasShape
import ru.hnau.androidutils.ui.drawer.ripple.RippleDrawer
import ru.hnau.androidutils.ui.drawer.shadow.drawer.ButtonShadowDrawer
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.LinearLayoutSeparator
import ru.hnau.androidutils.ui.view.label.Label
import ru.hnau.androidutils.ui.view.utils.apply.*
import ru.hnau.androidutils.ui.view.utils.apply.layout_params.applyFrameParams
import ru.hnau.androidutils.ui.view.utils.apply.layout_params.applyLinearParams
import ru.hnau.androidutils.ui.view.utils.createIsVisibleToUserProducer
import ru.hnau.androidutils.ui.view.utils.getDefaultMeasurement
import ru.hnau.androidutils.ui.view.utils.makeExactlyMeasureSpec
import ru.hnau.androidutils.ui.view.utils.setSoftwareRendering
import ru.hnau.androidutils.ui.view.utils.touch.TouchHandler


class ProductsViewItemView(
    context: Context,
    private val onProductClick: (Product) -> Unit
) : FrameLayout(
    context
) {

    companion object {

        private val PREFERRED_SIZE = dp192

    }

    var product: Product? = null
        set(value) {
            if (field != value && value != null) {
                field = value
                onProductChanged(value)
            }
        }


    private val iconView = ProductIconView(context)
        .applyLinearParams {
            setMatchParentWidth()
            setStretchedHeight()
        }

    private val titleView = Label(
        context = context,
        textColor = ColorManager.PRIMARY,
        textSize = SizeManager.TEXT_16,
        maxLines = 1,
        minLines = 1,
        gravity = HGravity.START_CENTER_VERTICAL
    )
        .applyPadding(SizeManager.SMALL_SEPARATION)

    private val colorLine = ColorLineView(context)
        .applyLinearParams {
            setMatchParentWidth()
            setMargins(SizeManager.EXTRA_SMALL_SEPARATION)
        }

    private val contentContainer = LinearLayout(context)
        .applyVerticalOrientation()
        .applyPadding(SizeManager.DEFAULT_SEPARATION + SizeManager.EXTRA_SMALL_SEPARATION)
        .addChild(iconView)
        .addChild(titleView)
        .addChild(colorLine)

    init {
        addView(ProductsViewButton(context) { product?.let(onProductClick) })
        addView(contentContainer)
    }

    private fun onProductChanged(product: Product) {
        colorLine.setColor(product.category.color)
        iconView.product = product
        titleView.text = product.name.toGetter()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val width = getDefaultMeasurement(widthMeasureSpec, PREFERRED_SIZE.getPxInt(context))
        val height = getDefaultMeasurement(heightMeasureSpec, PREFERRED_SIZE.getPxInt(context))
        super.onMeasure(
            makeExactlyMeasureSpec(width),
            makeExactlyMeasureSpec(height)
        )
    }

}