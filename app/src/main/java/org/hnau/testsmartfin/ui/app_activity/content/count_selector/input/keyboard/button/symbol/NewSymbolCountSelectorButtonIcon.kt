package org.hnau.testsmartfin.ui.app_activity.content.count_selector.input.keyboard.button.symbol

import android.content.Context
import android.graphics.Canvas
import android.graphics.ColorFilter
import android.graphics.Paint
import org.hnau.testsmartfin.ui.utils.ColorManager
import org.hnau.testsmartfin.ui.utils.SizeManager
import ru.hnau.androidutils.ui.drawables.DrawableOpacity
import ru.hnau.androidutils.ui.drawables.HDrawable


class NewSymbolCountSelectorButtonIcon(
    context: Context,
    private val symbol: String
) : HDrawable() {

    private val paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = ColorManager.PRIMARY.get(context)
        textAlign = Paint.Align.CENTER
        textSize = SizeManager.TEXT_24.getPx(context)
    }

    override fun draw(canvas: Canvas, width: Float, height: Float) {
        val cx = width / 2f
        val cy = (height + paint.textSize - paint.fontMetrics.descent) / 2f
        canvas.drawText(symbol, cx, cy, paint)
    }

    override fun getDrawableOpacity() =
        DrawableOpacity.TRANSLUCENT

    override fun setAlpha(alpha: Int) {
        paint.alpha = alpha
    }

    override fun setColorFilter(colorFilter: ColorFilter?) {
        paint.colorFilter = colorFilter
    }

}