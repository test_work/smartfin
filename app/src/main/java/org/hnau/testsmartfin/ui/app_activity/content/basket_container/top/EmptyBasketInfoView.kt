package org.hnau.testsmartfin.ui.app_activity.content.basket_container.top

import android.content.Context
import android.widget.LinearLayout
import org.hnau.testsmartfin.R
import org.hnau.testsmartfin.ui.utils.ColorManager
import org.hnau.testsmartfin.ui.utils.SizeManager
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.label.addLabel
import ru.hnau.androidutils.ui.view.utils.apply.*
import ru.hnau.androidutils.ui.view.utils.apply.layout_params.applyLinearParams


class EmptyBasketInfoView(
    context: Context
) : LinearLayout(
    context
) {

    init {

        applyVerticalOrientation()
        applyPadding(SizeManager.LARGE_SEPARATION)
        applyCenterGravity()

        addImageView { setImageResource(R.drawable.ic_basket) }
            .applyLinearParams()

        addLabel(
            text = StringGetter(R.string.empty_basket_info_title),
            gravity = HGravity.CENTER,
            textSize = SizeManager.TEXT_24,
            textColor = ColorManager.PRIMARY
        ) {
            applyVerticalPadding(SizeManager.DEFAULT_SEPARATION)
        }

        addLabel(
            text = StringGetter(R.string.empty_basket_info_text),
            gravity = HGravity.CENTER,
            textSize = SizeManager.TEXT_20,
            textColor = ColorManager.PRIMARY_T75
        )

    }

}