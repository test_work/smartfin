package org.hnau.testsmartfin.ui.app_activity.content.basket_container.top.items

import android.content.Context
import android.widget.LinearLayout
import org.hnau.testsmartfin.R
import org.hnau.testsmartfin.data.entity.BasketItem
import org.hnau.testsmartfin.ui.app_activity.content.basket_container.top.items.list.BasketItemsList
import org.hnau.testsmartfin.ui.utils.ColorManager
import org.hnau.testsmartfin.ui.utils.SizeManager
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.context_getters.dp_px.dp40
import ru.hnau.androidutils.context_getters.toGetter
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.label.addLabel
import ru.hnau.androidutils.ui.view.shadow_line.addBottomShadowLine
import ru.hnau.androidutils.ui.view.shadow_line.addTopShadowLine
import ru.hnau.androidutils.ui.view.utils.apply.*
import ru.hnau.androidutils.ui.view.utils.apply.layout_params.applyFrameParams
import ru.hnau.androidutils.ui.view.utils.apply.layout_params.applyLinearParams
import ru.hnau.jutils.producer.Producer
import ru.hnau.remote_teaching_android.ui.button.HButtonInfo
import ru.hnau.remote_teaching_android.ui.button.addHButton


class BasketItemsContainer(
    context: Context,
    items: Producer<List<BasketItem>>,
    onClearItemsButtonClick: () -> Unit
) : LinearLayout(
    context
) {

    init {

        applyVerticalOrientation()

        addHorizontalLayout {

            applyStartPadding(SizeManager.DEFAULT_SEPARATION)
            applyVerticalPadding(SizeManager.SMALL_SEPARATION)
            applyEndPadding(SizeManager.SMALL_SEPARATION)

            addLabel(
                text = "#34".toGetter(),
                minLines = 1,
                maxLines = 1,
                textColor = ColorManager.PRIMARY_T75,
                gravity = HGravity.START_CENTER_VERTICAL,
                textSize = SizeManager.TEXT_16
            ) {
                applyLinearParams {
                    setStretchedWidth()
                    setEndMargin(SizeManager.DEFAULT_SEPARATION)
                }
            }

            addHButton(
                text = StringGetter(R.string.clear_all),
                onClick = onClearItemsButtonClick,
                info = HButtonInfo(
                    height = dp40,
                    textColor = ColorManager.ADDITIONAL,
                    backgroundColor = ColorManager.TRANSPARENT
                )
            )

        }

        addFrameLayout {

            applyLinearParams {
                setMatchParentWidth()
                setStretchedHeight()
            }

            addChild(BasketItemsList(context, items))

            addTopShadowLine {
                applyFrameParams {
                    setMatchParentWidth()
                    setTopGravity()
                }
            }

            addBottomShadowLine {
                applyFrameParams {
                    setMatchParentWidth()
                    setBottomGravity()
                }
            }

        }

    }

}