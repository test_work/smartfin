package org.hnau.testsmartfin.data.db

import org.hnau.testsmartfin.data.entity.Product
import org.hnau.testsmartfin.data.ProductsLoader
import ru.hnau.androidutils.utils.ContextConnector


object ProductsDbCacheLevel {

    suspend fun load(): List<Product> {

        val db = DbManager.openDB(ContextConnector.context)

        var result = Product.loadFromDb(db)

        //Если продуктов в базе не оказалось, то загрузим их и сохраним в базу
        if (result.isEmpty()) {
            result = ProductsLoader.load()
            Product.saveToDb(db, result)
        }

        return result
    }

}