package org.hnau.testsmartfin.ui.app_activity.content.products_container.products_pager.products_view

import android.content.Context
import android.support.v7.widget.RecyclerView
import org.hnau.testsmartfin.data.entity.Product
import org.hnau.testsmartfin.ui.app_activity.content.products_container.products_pager.products_view.item_view.ProductsViewItemView


class ProductsViewViewHolder private constructor(
    private val contentView: ProductsViewItemView
) : RecyclerView.ViewHolder(
    contentView
) {

    companion object {

        fun create(
            context: Context,
            onProductClick: (Product) -> Unit
        ) =
            ProductsViewViewHolder(
                ProductsViewItemView(
                    context = context,
                    onProductClick = onProductClick
                )
            )

    }

    fun setProduct(product: Product) {
        contentView.product = product
    }

}