package org.hnau.testsmartfin.utils

import okhttp3.*
import ru.hnau.androidutils.utils.ContextConnector
import java.io.IOException
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine


object DataLoader {

    private val HTTP_CLIENT = OkHttpClient.Builder()
        .cache(Cache(ContextConnector.context.getCacheDir(), Long.MAX_VALUE))
        .build()

    suspend fun load(url: String) =
        suspendCoroutine<ByteArray> { continuation ->
            val request = Request.Builder().url(url).build()
            HTTP_CLIENT.newCall(request).enqueue(
                object : Callback {

                    override fun onFailure(call: Call, e: IOException) =
                        continuation.resumeWithException(e)

                    override fun onResponse(call: Call, response: Response) {
                        val bytes = response.body()?.bytes() ?: throw IOException("No data available")
                        continuation.resume(bytes)
                    }

                }
            )
        }

}