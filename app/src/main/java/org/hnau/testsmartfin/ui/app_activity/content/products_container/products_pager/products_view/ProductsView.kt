package org.hnau.testsmartfin.ui.app_activity.content.products_container.products_pager.products_view

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.google.android.flexbox.*
import org.hnau.testsmartfin.data.entity.Product
import ru.hnau.androidutils.ui.view.utils.createIsVisibleToUserProducer
import ru.hnau.jutils.ifTrue
import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.producer.extensions.observeWhen


class ProductsView(
    context: Context,
    productsProducer: Producer<List<Product>>,
    private val onProductClick: (Product) -> Unit
) : RecyclerView(
    context
) {

    companion object {

        private const val COLUMNS_COUNT = 5

    }

    private var productsList = emptyList<Product>()

    private val flexboxLayoutManager =
        FlexboxLayoutManager(context).apply {
            flexDirection = FlexDirection.ROW
        }

    private val productsAdapter =
        object : RecyclerView.Adapter<ProductsViewViewHolder>() {

            override fun onCreateViewHolder(
                viewGroup: ViewGroup,
                itemType: Int
            ) =
                ProductsViewViewHolder.create(
                    context = context,
                    onProductClick = onProductClick
                )

            override fun getItemCount() = productsList.size

            override fun onBindViewHolder(
                viewHolder: ProductsViewViewHolder,
                position: Int
            ) {
                viewHolder.setProduct(productsList[position])
            }

        }

    private val isVisibleToUserProducer =
        createIsVisibleToUserProducer()

    init {
        layoutManager = flexboxLayoutManager
        adapter = productsAdapter

        productsProducer.observeWhen(isVisibleToUserProducer) {
            (productsList == it).ifTrue { return@observeWhen }
            productsList = it
            productsAdapter.notifyDataSetChanged()
        }
    }

}