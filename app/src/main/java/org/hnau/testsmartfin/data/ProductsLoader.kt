package org.hnau.testsmartfin.data

import org.hnau.testsmartfin.data.entity.Product
import org.hnau.testsmartfin.data.entity.ProductCategory
import org.hnau.testsmartfin.utils.Amount
import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.coroutines.delay


object ProductsLoader {

    private val FAKE_PRODUCTS = listOf(
        Product(
            id = "beet",
            category = ProductCategory.BYELORUSSIA,
            logoUrl = "http://hnau.org/smartfin/beet.jpg",
            price = Amount(3),
            name = "Свекла"
        ),
        Product(
            id = "carrot",
            category = ProductCategory.RUSSIA,
            logoUrl = "http://hnau.org/smartfin/carrot.jpg",
            price = Amount(4),
            name = "Морковь"
        ),
        Product(
            id = "cucumber",
            category = ProductCategory.RUSSIA,
            logoUrl = "http://hnau.org/smartfin/cucumber.jpg",
            price = Amount(12),
            name = "Огурец"
        ),
        Product(
            id = "eggplant",
            category = ProductCategory.BYELORUSSIA,
            logoUrl = "http://hnau.org/smartfin/eggplant.jpg",
            price = Amount(15),
            name = "Баклажан"
        ),
        Product(
            id = "garlic",
            category = ProductCategory.RUSSIA,
            logoUrl = "http://hnau.org/smartfin/garlic.jpg",
            price = Amount(5),
            name = "Чеснок"
        ),
        Product(
            id = "lemon",
            category = ProductCategory.BYELORUSSIA,
            logoUrl = "http://hnau.org/smartfin/lemon.jpg",
            price = Amount(7),
            name = "Лимон"
        ),
        Product(
            id = "onion",
            category = ProductCategory.RUSSIA,
            logoUrl = "http://hnau.org/smartfin/onion.jpg",
            price = Amount(4),
            name = "Лук"
        ),
        Product(
            id = "radish",
            category = ProductCategory.RUSSIA,
            logoUrl = "http://hnau.org/smartfin/radish.jpg",
            price = Amount(11),
            name = "Редис"
        )
    )

    suspend fun load(): List<Product> {

        //Сделаем вид, что загружаем список продуктов из сети
        delay(TimeValue.SECOND * 5)

        return FAKE_PRODUCTS
    }

}