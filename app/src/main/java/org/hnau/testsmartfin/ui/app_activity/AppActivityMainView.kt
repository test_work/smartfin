package org.hnau.testsmartfin.ui.app_activity

import android.content.Context
import android.graphics.drawable.LayerDrawable
import android.widget.LinearLayout
import org.hnau.testsmartfin.R
import org.hnau.testsmartfin.ui.utils.ColorManager
import org.hnau.testsmartfin.ui.app_activity.content.AppActivityContentView
import org.hnau.testsmartfin.ui.app_activity.content.basket_container.BasketContainer
import org.hnau.testsmartfin.ui.utils.SizeManager
import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.context_getters.dp_px.dp128
import ru.hnau.androidutils.context_getters.dp_px.dp256
import ru.hnau.androidutils.context_getters.toGetter
import ru.hnau.androidutils.ui.drawables.layout_drawable.LayoutDrawable
import ru.hnau.androidutils.ui.utils.ScreenManager
import ru.hnau.androidutils.ui.utils.Side
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.header.addHeader
import ru.hnau.androidutils.ui.view.header.addHeaderTitle
import ru.hnau.androidutils.ui.view.header.button.addHeaderIconButton
import ru.hnau.androidutils.ui.view.shadow_line.addShadowLine
import ru.hnau.androidutils.ui.view.utils.apply.*
import ru.hnau.androidutils.ui.view.utils.apply.layout_params.applyFrameParams
import ru.hnau.androidutils.ui.view.utils.apply.layout_params.applyLinearParams


class AppActivityMainView(
    context: Context,
    onOpenMenuClick: () -> Unit
) : LinearLayout(
    context
) {

    init {
        applyHorizontalOrientation()

        val basketContainer = BasketContainer(context)
            .applyLinearParams {
                setMatchParentHeight()
                setStretchedWidth()
            }

        addFrameLayout {

            applyLinearParams {
                setMatchParentHeight()
                setZeroWidth()
                setWeight(2f)
            }

            addVerticalLayout {

                addHeader(
                    headerBackgroundColor = ColorManager.PRIMARY,
                    underStatusBar = true
                ) {

                    addHeaderIconButton(
                        icon = LayoutDrawable(
                            context = context,
                            initialContent = DrawableGetter(R.drawable.ic_menu)
                        ).toGetter(),
                        rippleDrawInfo = ColorManager.BG_ON_TRANSPARENT_RIPPLE_DRAW_INFO,
                        onClick = onOpenMenuClick
                    )

                    addHeaderTitle(
                        text = StringGetter(R.string.main_layer_title),
                        minLines = 1,
                        maxLines = 1,
                        textColor = ColorManager.BG,
                        gravity = HGravity.START_CENTER_VERTICAL,
                        textSize = SizeManager.TEXT_16
                    )

                }

                addView(
                    AppActivityContentView(context, basketContainer::onBasketItemAdded)
                        .applyLinearParams {
                            setMatchParentWidth()
                            setStretchedHeight()
                        }
                )

            }

            addShadowLine(fromSide = Side.END) {
                applyFrameParams {
                    setMatchParentHeight()
                    setEndGravity()
                }
            }

        }

        addView(basketContainer)

    }

}