package org.hnau.testsmartfin.ui.app_activity.content

import android.content.Context
import android.widget.LinearLayout
import org.hnau.testsmartfin.data.entity.BasketItem
import org.hnau.testsmartfin.data.entity.Product
import org.hnau.testsmartfin.ui.app_activity.AppActivity
import org.hnau.testsmartfin.ui.utils.ColorManager
import org.hnau.testsmartfin.ui.app_activity.content.count_selector.CountSelectorView
import org.hnau.testsmartfin.ui.app_activity.content.products_container.ProductsContainer
import ru.hnau.androidutils.ui.view.utils.apply.addChild
import ru.hnau.androidutils.ui.view.utils.apply.applyBackground
import ru.hnau.androidutils.ui.view.utils.apply.applyHorizontalOrientation
import ru.hnau.androidutils.ui.view.utils.apply.layout_params.applyLinearParams


class AppActivityContentView(
    context: Context,
    private val onBasketItemAdded: (BasketItem) -> Unit
) : LinearLayout(
    context
) {

    private val productsContainer = ProductsContainer(
        context = context,
        onProductClick = this::onProductClick
    )
        .applyLinearParams {
            setMatchParentHeight()
            setStretchedWidth()
        }

    init {
        applyBackground(ColorManager.BG)
        applyHorizontalOrientation()
        addChild(productsContainer)
    }

    private fun onProductClick(product: Product) {
        (context as AppActivity).showDialog {
            view(
                CountSelectorView(
                    context,
                    product,
                    this::close
                ) { weight ->
                    close()
                    onBasketItemAdded(BasketItem(product, weight))
                }
            )
        }
    }

}