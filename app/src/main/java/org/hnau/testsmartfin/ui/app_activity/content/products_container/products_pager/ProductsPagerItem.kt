package org.hnau.testsmartfin.ui.app_activity.content.products_container.products_pager

import org.hnau.testsmartfin.R
import org.hnau.testsmartfin.data.entity.Product
import org.hnau.testsmartfin.data.entity.ProductCategory
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.jutils.producer.Producer


sealed class ProductsPagerItem() {

    object All : ProductsPagerItem() {

        override val title = StringGetter(R.string.product_category_type_title_all)

        override fun prepareProductsProducer(
            productsProducer: Producer<List<Product>>
        ) = productsProducer

    }

    class Category(
        private val category: ProductCategory
    ) : ProductsPagerItem() {

        override val title = category.title

        override fun prepareProductsProducer(
            productsProducer: Producer<List<Product>>
        ) = productsProducer
            .map { products ->
                products.filter { product ->
                    product.category == category
                }
            }

    }

    abstract val title: StringGetter

    abstract fun prepareProductsProducer(
        productsProducer: Producer<List<Product>>
    ): Producer<List<Product>>

}