package org.hnau.testsmartfin.data.entity

import org.hnau.testsmartfin.utils.Weight


data class BasketItem(
    val product: Product,
    val weight: Weight
) {

    val amount = product.price * weight.gramms

}