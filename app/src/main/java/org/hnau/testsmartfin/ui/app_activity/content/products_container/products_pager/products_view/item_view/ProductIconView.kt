package org.hnau.testsmartfin.ui.app_activity.content.products_container.products_pager.products_view.item_view

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.widget.FrameLayout
import kotlinx.coroutines.Dispatchers
import org.hnau.testsmartfin.data.entity.Product
import org.hnau.testsmartfin.ui.utils.ColorManager
import org.hnau.testsmartfin.utils.DataLoader
import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.context_getters.dp_px.dp12
import ru.hnau.androidutils.context_getters.dp_px.dp128
import ru.hnau.androidutils.context_getters.dp_px.dp16
import ru.hnau.androidutils.context_getters.dp_px.dp160
import ru.hnau.androidutils.context_getters.toGetter
import ru.hnau.androidutils.coroutines.createUIJob
import ru.hnau.androidutils.ui.drawables.layout_drawable.LayoutType
import ru.hnau.androidutils.ui.drawables.layout_drawable.view.LayoutDrawableView
import ru.hnau.androidutils.ui.view.utils.apply.addView
import ru.hnau.androidutils.ui.view.utils.createIsVisibleToUserProducer
import ru.hnau.androidutils.ui.view.waiter.material.drawer.params.MaterialWaiterSize
import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.coroutines.delay
import ru.hnau.jutils.coroutines.launch
import ru.hnau.jutils.ifFalse
import ru.hnau.jutils.producer.locked_producer.SuspendLockedProducer
import java.lang.IllegalArgumentException
import java.lang.Math.pow
import kotlin.concurrent.thread
import kotlin.math.min


class ProductIconView(
    context: Context
) : FrameLayout(
    context
) {

    companion object {

        private val MIN_BITMAP_SIZE = dp128

        private val BITMAPS_CACHE = LinkedHashMap<String, Bitmap>()

    }

    private val iconView = LayoutDrawableView(
        context = context,
        initialLayoutType = LayoutType.Inside,
        initialContent = DrawableGetter.EMPTY
    )

    private val lockedProducer = SuspendLockedProducer()

    var product: Product? = null
        set(value) {
            if (field != value && value != null) {
                field = value
                onProductChanged(value)
            }
        }

    private val isVisibleToUserProducer =
        createIsVisibleToUserProducer()

    private val uiJob =
        createUIJob(isVisibleToUserProducer)

    init {
        addView(iconView)
        addView(
            ColorManager.createWaiterView(
                context,
                lockedProducer,
                MaterialWaiterSize.MEDIUM
            )
        )
    }

    private fun onProductChanged(product: Product) {
        uiJob.launchAfterStart {
            lockedProducer.executeLocked {
                try {
                    onProductChangedUnsafe(product)
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
        }
    }

    private suspend fun onProductChangedUnsafe(product: Product) {
        val url = product.logoUrl
        var bitmap = BITMAPS_CACHE[url]
        if (bitmap == null) {
            bitmap = decodeBitmap(DataLoader.load(url))
            BITMAPS_CACHE[url] = bitmap
            (product == this.product).ifFalse { return }
        }
        iconView.content = BitmapDrawable(bitmap).toGetter()
    }

    private fun decodeBitmap(bytes: ByteArray): Bitmap {

        val options = BitmapFactory.Options().apply { inJustDecodeBounds = true }
        BitmapFactory.decodeByteArray(bytes, 0, bytes.size, options)

        val bitmapSize = min(options.outWidth, options.outHeight).toDouble()
        val factor = bitmapSize / MIN_BITMAP_SIZE.getPx(context)
        val scale = (Math.log(factor) / Math.log(2.0)).toInt()

        val inSampleSize = pow(2.0, scale.toDouble()).toInt()
        val decodeOptions = BitmapFactory.Options().apply { this.inSampleSize = inSampleSize }
        val bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.size, decodeOptions)
        return bitmap
    }


}