package org.hnau.testsmartfin.ui.app_activity.content.products_container.products_pager.products_view.item_view

import android.content.Context
import android.graphics.Canvas
import android.view.MotionEvent
import android.view.View
import org.hnau.testsmartfin.data.entity.Product
import org.hnau.testsmartfin.ui.utils.ColorManager
import org.hnau.testsmartfin.ui.utils.SizeManager
import ru.hnau.androidutils.context_getters.dp_px.dp128
import ru.hnau.androidutils.context_getters.dp_px.dp8
import ru.hnau.androidutils.ui.bounds_producer.createBoundsProducer
import ru.hnau.androidutils.ui.canvas_shape.RoundCornersRectCanvasShape
import ru.hnau.androidutils.ui.drawer.ripple.RippleDrawer
import ru.hnau.androidutils.ui.drawer.shadow.drawer.ButtonShadowDrawer
import ru.hnau.androidutils.ui.view.utils.apply.applyPadding
import ru.hnau.androidutils.ui.view.utils.createIsVisibleToUserProducer
import ru.hnau.androidutils.ui.view.utils.setSoftwareRendering
import ru.hnau.androidutils.ui.view.utils.touch.TouchHandler


class ProductsViewButton(
    context: Context,
    onClick: () -> Unit
) : View(
    context
) {

    companion object {

        private val CORNERS_RADIUS = dp8

    }

    private val boundsProducer =
        createBoundsProducer()

    private val canvasShape = RoundCornersRectCanvasShape(
        context = context,
        boundsProducer = boundsProducer,
        cornersRadius = CORNERS_RADIUS
    )

    private val touchHandler = TouchHandler(
        canvasShape = canvasShape,
        onClick = onClick
    )

    private val isVisibleToUserProducer =
        createIsVisibleToUserProducer()

    private val buttonShadowDrawer = ButtonShadowDrawer(
        animatingView = this,
        animatingViewIsVisibleToUser = isVisibleToUserProducer,
        shadowInfo = ColorManager.DEFAULT_BUTTON_SHADOW_INFO,
        canvasShape = canvasShape,
        touchHandler = touchHandler
    )

    private val rippleDrawer = RippleDrawer(
        animatingView = this,
        animatingViewIsVisibleToUser = isVisibleToUserProducer,
        canvasShape = canvasShape,
        touchHandler = touchHandler,
        rippleDrawInfo = ColorManager.FG_ON_BG_RIPPLE_DRAW_INFO
    )

    init {
        setSoftwareRendering()
        applyPadding(SizeManager.DEFAULT_SEPARATION)
    }

    override fun draw(canvas: Canvas) {
        super.draw(canvas)
        buttonShadowDrawer.draw(canvas)
        rippleDrawer.draw(canvas)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        super.onTouchEvent(event)
        touchHandler.handle(event)
        return true
    }

}