package org.hnau.testsmartfin.data.entity

import android.content.ContentValues
import android.database.sqlite.SQLiteDatabase
import org.hnau.testsmartfin.utils.Amount
import org.hnau.testsmartfin.utils.Weight
import java.util.*


data class Product(
    val id: String,
    val name: String,
    val logoUrl: String,
    val category: ProductCategory,
    val price: Amount
) {

    companion object {

        private const val TABLE_NAME = "product"

        private const val ID_COLUMN_NAME = "id"
        private const val NAME_COLUMN_NAME = "name"
        private const val LOGO_URL_COLUMN_NAME = "logo_url"
        private const val CATEGORY_COLUMN_NAME = "category"
        private const val PRICE_COLUMN_NAME = "price"

        private val COLUMNS = arrayOf(
            ID_COLUMN_NAME,
            NAME_COLUMN_NAME,
            LOGO_URL_COLUMN_NAME,
            CATEGORY_COLUMN_NAME,
            PRICE_COLUMN_NAME
        )

        private const val CREATE_TABLE_SQL = """
            CREATE TABLE $TABLE_NAME
            (
                $ID_COLUMN_NAME TEXT PRIMARY KEY,
                $NAME_COLUMN_NAME TEXT,
                $LOGO_URL_COLUMN_NAME TEXT,
                $CATEGORY_COLUMN_NAME TEXT,
                $PRICE_COLUMN_NAME INT
            )
        """

        fun createDbTable(db: SQLiteDatabase) {
            db.execSQL(CREATE_TABLE_SQL)
        }

        fun loadFromDb(db: SQLiteDatabase): List<Product> {

            val cursor = db.query(
                TABLE_NAME,
                COLUMNS, null, null, null, null, null
            )
            if (cursor.count <= 0) {
                cursor.close()
                return emptyList()
            }

            val idColumnIndex = cursor.getColumnIndexOrThrow(ID_COLUMN_NAME)
            val nameColumnIndex = cursor.getColumnIndexOrThrow(NAME_COLUMN_NAME)
            val logoUrlColumnIndex = cursor.getColumnIndexOrThrow(LOGO_URL_COLUMN_NAME)
            val categoryColumnIndex = cursor.getColumnIndexOrThrow(CATEGORY_COLUMN_NAME)
            val priceColumnIndex = cursor.getColumnIndexOrThrow(PRICE_COLUMN_NAME)

            val result = LinkedList<Product>()
            while (cursor.moveToNext()) {
                result.add(
                    Product(
                        id = cursor.getString(idColumnIndex),
                        name = cursor.getString(nameColumnIndex),
                        logoUrl = cursor.getString(logoUrlColumnIndex),
                        category = ProductCategory.valueOf(
                            cursor.getString(
                                categoryColumnIndex
                            )
                        ),
                        price = Amount(cursor.getLong(priceColumnIndex))
                    )
                )
            }

            cursor.close()
            return result
        }

        fun saveToDb(db: SQLiteDatabase, products: List<Product>) {
            products.forEach { product ->
                val contentValues = ContentValues().apply {
                    put(ID_COLUMN_NAME, product.id)
                    put(NAME_COLUMN_NAME, product.name)
                    put(LOGO_URL_COLUMN_NAME, product.logoUrl)
                    put(CATEGORY_COLUMN_NAME, product.category.name)
                    put(PRICE_COLUMN_NAME, product.price.kops)
                }
                db.insert(TABLE_NAME, null, contentValues)
            }
        }

    }

    val priceByKg: Amount
        get() = price * Weight.KG_IN_G

}