package org.hnau.testsmartfin.ui.app_activity.content.products_container.products_pager

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.view.View
import android.view.ViewGroup
import org.hnau.testsmartfin.data.entity.Product
import org.hnau.testsmartfin.data.entity.ProductCategory
import org.hnau.testsmartfin.ui.app_activity.content.products_container.products_pager.products_view.ProductsView
import org.hnau.testsmartfin.ui.utils.ColorManager
import ru.hnau.androidutils.ui.view.utils.apply.applyBackground
import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.toSingleItemList


class ProductsViewPager(
    context: Context,
    private val productsProducer: Producer<List<Product>>,
    private val onProductClick: (Product) -> Unit
) : ViewPager(
    context
) {

    companion object {

        private val ITEMS =
            ProductsPagerItem.All.toSingleItemList() +
                    ProductCategory.values().map {
                        ProductsPagerItem.Category(
                            it
                        )
                    }

    }

    private val pagesAdapter = object : PagerAdapter() {

        override fun getPageTitle(position: Int) =
            ITEMS[position].title.get(context)

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val item = ITEMS[position]
            val view =
                ProductsView(
                    context = context,
                    onProductClick = onProductClick,
                    productsProducer = item.prepareProductsProducer(productsProducer)
                )
            container.addView(view)
            return view
        }

        override fun destroyItem(container: ViewGroup, position: Int, any: Any) {
            container.removeView(any as View)
        }

        override fun isViewFromObject(view: View, any: Any) =
            view == any

        override fun getCount() = ITEMS.size

    }

    init {
        applyBackground(ColorManager.BG)
        adapter = pagesAdapter
    }

}