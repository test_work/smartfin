package org.hnau.testsmartfin.ui.app_activity.content.products_container.products_pager.products_view.item_view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.view.View
import org.hnau.testsmartfin.ui.utils.ColorManager
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.dp_px.dp4
import ru.hnau.androidutils.context_getters.dp_px.dp8
import ru.hnau.androidutils.ui.bounds_producer.createBoundsProducer
import ru.hnau.androidutils.ui.canvas_shape.RoundSidesRectCanvasShape
import ru.hnau.androidutils.ui.view.utils.getDefaultMeasurement
import ru.hnau.androidutils.ui.view.utils.getMaxMeasurement
import ru.hnau.androidutils.ui.view.utils.verticalPaddingSum


class ColorLineView(
    context: Context
) : View(
    context
) {

    companion object {

        private val PREFERRED_HEIGHT = dp4

    }

    private val paint = Paint(Paint.ANTI_ALIAS_FLAG)

    private val boundsProducer =
        createBoundsProducer()

    private val canvasShape =
        RoundSidesRectCanvasShape(boundsProducer)

    fun setColor(color: ColorGetter) {
        paint.color = color.get(context)
    }

    override fun draw(canvas: Canvas) {
        canvasShape.draw(canvas, paint)
        super.draw(canvas)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val width = getMaxMeasurement(widthMeasureSpec, 0)
        val height = getDefaultMeasurement(heightMeasureSpec, PREFERRED_HEIGHT.getPxInt(context) + verticalPaddingSum)
        setMeasuredDimension(width, height)
    }

}