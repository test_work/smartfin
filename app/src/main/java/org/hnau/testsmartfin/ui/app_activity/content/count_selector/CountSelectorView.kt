package org.hnau.testsmartfin.ui.app_activity.content.count_selector

import android.content.Context
import android.widget.LinearLayout
import org.hnau.testsmartfin.R
import org.hnau.testsmartfin.data.entity.Product
import org.hnau.testsmartfin.ui.utils.ColorManager
import org.hnau.testsmartfin.ui.utils.SizeManager
import org.hnau.testsmartfin.ui.app_activity.content.count_selector.input.CountSelectorInput
import org.hnau.testsmartfin.utils.Weight
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.context_getters.toGetter
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.label.Label
import ru.hnau.androidutils.ui.view.utils.apply.*
import ru.hnau.androidutils.ui.view.utils.apply.layout_params.applyLinearParams
import ru.hnau.remote_teaching_android.ui.button.HButton
import ru.hnau.remote_teaching_android.ui.button.HButtonInfo


class CountSelectorView(
    context: Context,
    private val product: Product,
    onCancelClick: () -> Unit,
    onAddClick: (Weight) -> Unit
) : LinearLayout(
    context
) {

    private val amountLabel = createLabel(
        color = ColorManager.PRIMARY_T75
    )

    private val weightLabel = createLabel(
        color = ColorManager.PRIMARY_T75
    )

    private val titleView = LinearLayout(context)
        .applyHorizontalOrientation()
        .applyPadding(SizeManager.SMALL_SEPARATION)
        .addChild(
            createLabel(
                gravity = HGravity.START_CENTER_VERTICAL,
                color = ColorManager.PRIMARY,
                text = product.name.toGetter()
            )
                .applyLinearParams {
                    setStretchedWidth()
                    setEndMargin(SizeManager.DEFAULT_SEPARATION)
                }
        )
        .addChild(amountLabel)
        .addChild(
            createLabel(
                color = ColorManager.PRIMARY_T75,
                text = " / ".toGetter()
            )
        )
        .addChild(weightLabel)

    private val inputView =
        CountSelectorInput(context)

    private val cancelButton = HButton(
        context = context,
        text = StringGetter(R.string.dialog_cancel),
        info = HButtonInfo.SMALL_PRIMARY_BORDER,
        onClick = onCancelClick
    )

    private val addButton = HButton(
        context = context,
        text = StringGetter(R.string.dialog_add),
        info = HButtonInfo.SMALL_PRIMARY_BACKGROUND,
        onClick = { onAddClick(weight) },
        enable = inputView.weight.map { it.gramms > 0 }
    )

    private val amount = inputView.weight
        .map { product.price * it.gramms }

    private var weight = Weight.EMPTY

    init {
        applyVerticalOrientation()

        addView(titleView)
        addView(inputView)

        addHorizontalLayout {
            applyEndGravity()
            applyTopPadding(SizeManager.DEFAULT_SEPARATION)
            addChild(cancelButton)
            addChild(addButton)
        }

        inputView.weight.attach { weight ->
            this.weight = weight
            weightLabel.text = weight.toUiString(false)
        }

        amount.attach { amount ->
            amountLabel.text = amount.toUiString(false)
        }
    }

    private fun createLabel(
        color: ColorGetter,
        gravity: HGravity = HGravity.CENTER,
        text: StringGetter = StringGetter.EMPTY
    ) = Label(
        context = context,
        gravity = gravity,
        minLines = 1,
        maxLines = 1,
        textSize = SizeManager.TEXT_16,
        textColor = color,
        initialText = text
    )


}