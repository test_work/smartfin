package org.hnau.testsmartfin.ui.app_activity

import android.os.Bundle
import org.hnau.testsmartfin.ui.utils.ColorManager
import ru.hnau.androidutils.ui.TransparentStatusBarActivity
import ru.hnau.androidutils.ui.utils.Side
import ru.hnau.androidutils.ui.view.layer.manager.LayerManager
import ru.hnau.androidutils.ui.view.layer.preset.dialog.DialogLayer
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.material.MaterialDialogView
import ru.hnau.androidutils.ui.view.layer.transaction.TransactionInfo


class AppActivity : TransparentStatusBarActivity() {

    override fun getIsStatusBarIconsLight() = true

    private val layerManager: LayerManager by lazy {
        LayerManager(this).apply {
            showLayer(MainLayer(this@AppActivity))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layerManager)
    }

    override fun onBackPressed() {
        if (layerManager.handleGoBack()) {
            return
        }
        super.onBackPressed()
    }


    fun showDialog(
        dialogViewConfigurator: MaterialDialogView.() -> Unit
    ) {
        val dialogBuilder = MaterialDialogView.create(
            info = ColorManager.MATERIAL_DIALOG_VIEW_INFO,
            dialogViewConfigurator = dialogViewConfigurator
        )
        layerManager.showLayer(
            layer = DialogLayer.create(
                context = layerManager.viewContext,
                dialogViewBuilder = dialogBuilder
            ),
            transactionInfo = TransactionInfo(
                emersionSide = Side.BOTTOM
            )
        )
    }

}