package org.hnau.testsmartfin.ui.app_activity

import android.content.Context
import android.support.v4.widget.DrawerLayout
import android.view.Gravity
import ru.hnau.androidutils.ui.view.layer.layer.Layer
import ru.hnau.androidutils.ui.view.utils.MATCH_PARENT
import ru.hnau.androidutils.ui.view.utils.WRAP_CONTENT


class MainLayer(
    context: Context
) : DrawerLayout(
    context
), Layer {

    override val view = this

    private val menuView =
        AppActivityMenuView(
            context = context,
            onItemClick = this::closeMenu
        ).apply {
            layoutParams = DrawerLayout.LayoutParams(
                WRAP_CONTENT,
                MATCH_PARENT,
                Gravity.START or Gravity.LEFT
            )
        }

    init {
        addView(AppActivityMainView(context, this::openMenu))
        addView(menuView)
    }

    override fun handleGoBack(): Boolean {
        if (isDrawerOpen(menuView)) {
            closeMenu()
            return true
        }

        return false
    }

    private fun openMenu() =
        openDrawer(menuView)

    private fun closeMenu(): Unit =
        closeDrawer(menuView)

}