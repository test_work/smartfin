package org.hnau.testsmartfin.ui.app_activity.content.count_selector.input.keyboard.button.symbol

import android.content.Context
import org.hnau.testsmartfin.ui.app_activity.content.count_selector.input.keyboard.button.CountSelectorButton
import ru.hnau.androidutils.context_getters.toGetter
import ru.hnau.jutils.producer.Producer


class NewSymbolCountSelectorButton(
    context: Context,
    symbol: String,
    value: Producer<String>,
    valueIsCorrectChecker: (String) -> Boolean,
    valueIsChanged: (String) -> Unit
) : CountSelectorButton(
    context = context,
    icon = NewSymbolCountSelectorButtonIcon(
        context = context,
        symbol = symbol
    ).toGetter(),
    value = value,
    valueIsCorrectChecker = valueIsCorrectChecker,
    valueOnClickConverter = { it + symbol },
    valueIsChanged = valueIsChanged
)