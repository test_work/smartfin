package org.hnau.testsmartfin.ui.app_activity.content.basket_container.bottom

import android.content.Context
import android.widget.LinearLayout
import org.hnau.testsmartfin.R
import org.hnau.testsmartfin.data.entity.BasketItem
import org.hnau.testsmartfin.ui.utils.ColorManager
import org.hnau.testsmartfin.ui.utils.SizeManager
import org.hnau.testsmartfin.utils.Amount
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.label.Label
import ru.hnau.androidutils.ui.view.label.addLabel
import ru.hnau.androidutils.ui.view.utils.apply.*
import ru.hnau.androidutils.ui.view.utils.apply.layout_params.applyLinearParams
import ru.hnau.androidutils.ui.view.utils.createIsVisibleToUserProducer
import ru.hnau.androidutils.utils.longToast
import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.producer.extensions.mapIsEmpty
import ru.hnau.jutils.producer.extensions.not
import ru.hnau.jutils.producer.extensions.observeWhen


class BasketContainerBottomView(
    context: Context,
    items: Producer<List<BasketItem>>,
    onPay: () -> Unit
) : LinearLayout(
    context
) {

    private val isVisibleToUserProducer =
        createIsVisibleToUserProducer()

    init {
        applyVerticalOrientation()
        applyPadding(SizeManager.DEFAULT_SEPARATION)

        addHorizontalLayout {

            applyHorizontalPadding(SizeManager.DEFAULT_SEPARATION)
            applyTopPadding(SizeManager.DEFAULT_SEPARATION)
            applyBottomPadding(SizeManager.SMALL_SEPARATION)

            addLabel(
                text = StringGetter(R.string.total_amount),
                minLines = 1,
                maxLines = 1,
                textColor = ColorManager.PRIMARY,
                gravity = HGravity.START_CENTER_VERTICAL,
                textSize = SizeManager.TEXT_16
            ) {
                applyLinearParams {
                    setStretchedWidth()
                }
            }

            val totalAmountView = Label(
                context = context,
                minLines = 1,
                maxLines = 1,
                textColor = ColorManager.PRIMARY,
                gravity = HGravity.START_CENTER_VERTICAL,
                textSize = SizeManager.TEXT_16
            )

            addView(totalAmountView)

            items.observeWhen(isVisibleToUserProducer) { items ->
                val totalAmount = items.fold(Amount.ZERO) { acc, item -> acc + item.amount }
                totalAmountView.text = totalAmount.toUiString(true)
            }

        }

        addChild(
            PayButton(
                context = context,
                enable = items.mapIsEmpty().not(),
                onClick = {
                    longToast(StringGetter(R.string.pay_success))
                    onPay()
                }
            )
        )
    }

}