package org.hnau.testsmartfin.ui.app_activity.content.count_selector.input

import android.content.Context
import android.view.View
import android.widget.LinearLayout
import org.hnau.testsmartfin.R
import org.hnau.testsmartfin.ui.utils.ColorManager
import org.hnau.testsmartfin.ui.utils.SizeManager
import org.hnau.testsmartfin.ui.app_activity.content.count_selector.input.keyboard.CountSelectorKeyboard
import org.hnau.testsmartfin.utils.Weight
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.context_getters.dp_px.dp2
import ru.hnau.androidutils.context_getters.toGetter
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.LinearLayoutSeparator
import ru.hnau.androidutils.ui.view.label.Label
import ru.hnau.androidutils.ui.view.utils.apply.*
import ru.hnau.androidutils.ui.view.utils.apply.layout_params.applyLinearParams
import ru.hnau.androidutils.ui.view.utils.getDefaultMeasurement
import ru.hnau.androidutils.ui.view.utils.getMaxMeasurement
import ru.hnau.jutils.producer.StateProducerSimple
import ru.hnau.jutils.takeIfNotEmpty


class CountSelectorInput(
    context: Context
) : LinearLayout(
    context
) {

    companion object {

        private val MAX_FIRST_VALUE_PART_LENGTH = 5
        private val MAX_SECOND_VALUE_PART_LENGTH = 2
        private val KG_SUFFIX = StringGetter(R.string.kg)

    }

    private val valueProducer =
        StateProducerSimple("")

    val weight =
        valueProducer.map { value ->
            value
                .takeIfNotEmpty()
                ?.let { Weight((it.toFloat() * 1000).toLong()) }
                ?: Weight.EMPTY
        }

    val stringValue =
        valueProducer.map { value ->
            "$value ${KG_SUFFIX.get(context)}".toGetter()
        }

    private val valueLabel = Label(
        context = context,
        maxLines = 1,
        minLines = 1,
        textSize = SizeManager.TEXT_24,
        textColor = ColorManager.PRIMARY,
        gravity = HGravity.CENTER
    )
        .applyHorizontalPadding(SizeManager.LARGE_SEPARATION)
        .applyVerticalPadding(SizeManager.DEFAULT_SEPARATION)

    private val kgSuffix = context

    init {
        applyVerticalOrientation()
        addChild(valueLabel)
        /*addChild(
            View(context)
                .applyBackground(ColorManager.PRIMARY)
                .applyLinearParams {
                    setMatchParentWidth()
                    setHeight(dp2)
                    setHorizontalMargins(SizeManager.LARGE_SEPARATION)
                    setBottomMargin(SizeManager.DEFAULT_SEPARATION)
                }
        )*/
        addChild(
            CountSelectorKeyboard(
                context = context,
                valueIsCorrectChecker = this::checkValueIsCorrect,
                value = valueProducer,
                valueIsChanged = valueProducer::updateState
            )
        )

        stringValue
            .attach { valueLabel.text = it }
    }

    private fun checkValueIsCorrect(value: String): Boolean {

        if (value == CountSelectorKeyboard.EMPTY_PLACEHOLDER) {
            return false
        }

        if (value.isEmpty() || value == "0") {
            return true
        }

        if (value[0] == '0' && !value.startsWith("0" + CountSelectorKeyboard.DOT)) {
            return false
        }

        val dotsCount = value.count { it == CountSelectorKeyboard.DOT }
        if (dotsCount > 1) {
            return false
        }

        if (value[0] == CountSelectorKeyboard.DOT) {
            return false
        }

        val parts = value.split(CountSelectorKeyboard.DOT)

        val firstPart = parts[0]
        if (firstPart.length > MAX_FIRST_VALUE_PART_LENGTH) {
            return false
        }

        val secondPart = parts.getOrNull(1) ?: ""
        if (secondPart.length > MAX_SECOND_VALUE_PART_LENGTH) {
            return false
        }

        return true
    }

}