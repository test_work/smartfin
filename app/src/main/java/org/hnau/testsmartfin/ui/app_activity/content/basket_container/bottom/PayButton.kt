package org.hnau.testsmartfin.ui.app_activity.content.basket_container.bottom

import android.content.Context
import android.graphics.Canvas
import android.view.MotionEvent
import android.widget.FrameLayout
import android.widget.LinearLayout
import org.hnau.testsmartfin.R
import org.hnau.testsmartfin.ui.utils.ColorManager
import org.hnau.testsmartfin.ui.utils.SizeManager
import ru.hnau.androidutils.animations.smooth
import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.bounds_producer.createBoundsProducer
import ru.hnau.androidutils.ui.canvas_shape.RoundSidesRectCanvasShape
import ru.hnau.androidutils.ui.drawables.layout_drawable.view.addLayoutDrawableView
import ru.hnau.androidutils.ui.drawer.ripple.RippleDrawer
import ru.hnau.androidutils.ui.drawer.ripple.info.RippleDrawInfo
import ru.hnau.androidutils.ui.drawer.shadow.drawer.ButtonShadowDrawer
import ru.hnau.androidutils.ui.drawer.shadow.drawer.ShadowDrawer
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.label.addLabel
import ru.hnau.androidutils.ui.view.utils.apply.addView
import ru.hnau.androidutils.ui.view.utils.apply.applyHorizontalOrientation
import ru.hnau.androidutils.ui.view.utils.apply.applyPadding
import ru.hnau.androidutils.ui.view.utils.apply.layout_params.applyLinearParams
import ru.hnau.androidutils.ui.view.utils.createIsVisibleToUserProducer
import ru.hnau.androidutils.ui.view.utils.scroll.view.createIsScrolledToBottomProducer
import ru.hnau.androidutils.ui.view.utils.setSoftwareRendering
import ru.hnau.androidutils.ui.view.utils.touch.TouchHandler
import ru.hnau.jutils.getFloatInterFloats
import ru.hnau.jutils.ifFalse
import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.producer.extensions.observeWhen
import ru.hnau.jutils.producer.extensions.toFloat


class PayButton(
    context: Context,
    enable: Producer<Boolean>,
    onClick: () -> Unit
) : FrameLayout(
    context
) {

    private val boundsProducer =
        createBoundsProducer()

    private val canvasShape =
        RoundSidesRectCanvasShape(boundsProducer)

    private val touchHandler = TouchHandler(
        canvasShape = canvasShape,
        onClick = onClick
    )

    private val isVisibleToUserProducer =
        createIsVisibleToUserProducer()

    private val rippleDrawer = RippleDrawer(
        animatingView = this,
        animatingViewIsVisibleToUser = isVisibleToUserProducer,
        rippleDrawInfo = RippleDrawInfo(
            backgroundColor = ColorManager.ADDITIONAL,
            color = ColorManager.WHITE,
            rippleInfo = ColorManager.RIPPLE_INFO,
            rippleAlpha = ColorManager.DRAW_RIPPLE_ALPHA
        ),
        touchHandler = touchHandler,
        canvasShape = canvasShape
    )

    private val shadowDrawer = ButtonShadowDrawer(
        animatingView = this,
        animatingViewIsVisibleToUser = isVisibleToUserProducer,
        canvasShape = canvasShape,
        touchHandler = touchHandler,
        shadowInfo = ColorManager.DEFAULT_BUTTON_SHADOW_INFO
    )

    private var isEnableProducer =
        enable.observeWhen(isVisibleToUserProducer)

    private var enable = false

    private val contentContainer = LinearLayout(context)
        .applyHorizontalOrientation()
        .applyPadding(SizeManager.DEFAULT_SEPARATION, SizeManager.SMALL_SEPARATION)
        .addLayoutDrawableView(
            content = DrawableGetter(R.drawable.ic_card)
        )
        .addLabel(
            text = StringGetter(R.string.dialog_pay).toUpperCase(),
            textSize = SizeManager.TEXT_16,
            gravity = HGravity.CENTER,
            textColor = ColorManager.BG,
            maxLines = 1,
            minLines = 1
        ) {
            applyLinearParams { setStretchedWidth() }
        }

    init {
        applyPadding(
            shadowDrawer.extraSize.start,
            shadowDrawer.extraSize.top,
            shadowDrawer.extraSize.start,
            shadowDrawer.extraSize.end
        )
        setSoftwareRendering()

        addView(contentContainer)

        isEnableProducer.attach { this.enable = it }
        isEnableProducer
            .toFloat()
            .smooth()
            .attach { visibility ->
                alpha = getFloatInterFloats(0.25f, 1f, visibility)
            }
    }

    override fun dispatchDraw(canvas: Canvas) {
        shadowDrawer.draw(canvas)
        rippleDrawer.draw(canvas)
        super.dispatchDraw(canvas)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        super.onTouchEvent(event)
        enable.ifFalse { return true }
        touchHandler.handle(event)
        return true
    }


}