package org.hnau.testsmartfin.data.entity

import org.hnau.testsmartfin.R
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.StringGetter


enum class ProductCategory(
    val title: StringGetter,
    val color: ColorGetter
) {

    RUSSIA(
        title = StringGetter(R.string.product_category_type_title_russia),
        color = ColorGetter.byResId(R.color.product_category_russia)
    ),

    BYELORUSSIA(
        title = StringGetter(R.string.product_category_type_title_byelorussia),
        color = ColorGetter.byResId(R.color.product_category_byelorussia)
    );

}