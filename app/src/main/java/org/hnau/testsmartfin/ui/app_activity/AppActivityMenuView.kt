package org.hnau.testsmartfin.ui.app_activity

import android.content.Context
import android.widget.LinearLayout
import org.hnau.testsmartfin.R
import org.hnau.testsmartfin.ui.utils.ColorManager
import org.hnau.testsmartfin.ui.utils.SizeManager
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.context_getters.dp_px.*
import ru.hnau.androidutils.ui.drawer.ripple.info.RippleDrawInfo
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.clickable.ClickableLabel
import ru.hnau.androidutils.ui.view.prefered_size_view.PreferredSizeView
import ru.hnau.androidutils.ui.view.utils.apply.*
import ru.hnau.androidutils.ui.view.utils.apply.layout_params.applyLinearParams


class AppActivityMenuView(
    context: Context,
    private val onItemClick: () -> Unit
) : LinearLayout(
    context
) {

    init {
        applyVerticalPadding(dp64)
        applyVerticalOrientation()
        applyBackground(ColorGetter.DKGRAY)
        repeat(10) { number -> addView(createItemView(number)) }
        addView(PreferredSizeView.fixed(context, dp128, dp8))
    }

    private fun createItemView(number: Int) =
        ClickableLabel(
            context = context,
            rippleDrawInfo = ColorManager.BG_ON_TRANSPARENT_RIPPLE_DRAW_INFO,
            onClick = onItemClick,
            textColor = ColorManager.BG,
            gravity = HGravity.START_CENTER_VERTICAL,
            maxLines = 1,
            minLines = 1,
            initialText = StringGetter(R.string.manu_item_title, number.plus(1).toString()),
            textSize = SizeManager.TEXT_16
        )
            .applyLinearParams { setWidth(dp256) }
            .applyPadding(SizeManager.DEFAULT_SEPARATION)

}