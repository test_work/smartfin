package org.hnau.testsmartfin.ui.app_activity.content.basket_container.top

import android.content.Context
import android.view.View
import org.hnau.testsmartfin.data.entity.BasketItem
import org.hnau.testsmartfin.ui.app_activity.content.basket_container.top.items.BasketItemsContainer
import ru.hnau.androidutils.ui.utils.Side
import ru.hnau.androidutils.ui.view.view_presenter.PresenterView
import ru.hnau.androidutils.ui.view.view_presenter.PresentingViewInfo
import ru.hnau.androidutils.ui.view.view_presenter.PresentingViewProperties
import ru.hnau.jutils.handle
import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.producer.extensions.filterUnique
import ru.hnau.jutils.producer.extensions.mapIsEmpty


object BasketInfoContainer {

    private val PRESENTING_VIEW_PROPERTIES = PresentingViewProperties(
        fromSide = Side.BOTTOM,
        scrollFactor = 0.5f
    )

    fun create(
        context: Context,
        items: Producer<List<BasketItem>>,
        onClearItemsButtonClick: () -> Unit
    ): View {

        val emptyInfoView: PresentingViewInfo by lazy {
            PresentingViewInfo(
                view = EmptyBasketInfoView(context),
                properties = PRESENTING_VIEW_PROPERTIES
            )
        }

        val itemsView: PresentingViewInfo by lazy {
            PresentingViewInfo(
                view = BasketItemsContainer(
                    context,
                    items,
                    onClearItemsButtonClick
                ),
                properties = PRESENTING_VIEW_PROPERTIES
            )
        }

        return PresenterView(
            context = context,
            presentingViewProducer = items
                .mapIsEmpty()
                .filterUnique()
                .map { isEmpty ->
                    isEmpty.handle(
                        onTrue = { emptyInfoView },
                        onFalse = { itemsView }
                    )
                }
        )
    }


}