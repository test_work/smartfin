package org.hnau.testsmartfin.ui.app_activity.content.basket_container.top.items.list

import android.content.Context
import org.hnau.testsmartfin.data.entity.BasketItem
import org.hnau.testsmartfin.ui.utils.ColorManager
import org.hnau.testsmartfin.ui.utils.SizeManager
import ru.hnau.androidutils.context_getters.dp_px.dp1
import ru.hnau.androidutils.context_getters.dp_px.dp2
import ru.hnau.androidutils.ui.view.list.base.BaseList
import ru.hnau.androidutils.ui.view.list.base.BaseListItemsDivider
import ru.hnau.androidutils.ui.view.list.base.BaseListOrientation
import ru.hnau.jutils.producer.Producer


class BasketItemsList(
    context: Context,
    items: Producer<List<BasketItem>>
) : BaseList<BasketItem>(
    context = context,
    itemsProducer = items,
    viewWrappersCreator = { BasketItemsListItemView(context) },
    itemsDecoration = BaseListItemsDivider(
        context = context,
        color = ColorManager.PRIMARY_T75,
        size = dp1,
        paddingEnd = SizeManager.DEFAULT_SEPARATION,
        paddingStart = SizeManager.DEFAULT_SEPARATION
    ),
    orientation = BaseListOrientation.VERTICAL
)