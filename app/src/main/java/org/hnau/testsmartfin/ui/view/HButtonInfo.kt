package ru.hnau.remote_teaching_android.ui.button

import org.hnau.testsmartfin.ui.utils.ColorManager
import org.hnau.testsmartfin.ui.utils.SizeManager
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.dp_px.*
import ru.hnau.androidutils.ui.drawer.border.BorderInfo
import ru.hnau.androidutils.ui.drawer.ripple.info.RippleDrawInfo
import ru.hnau.androidutils.ui.drawer.shadow.info.ButtonShadowInfo


data class HButtonInfo(
    val textSize: DpPxGetter = SizeManager.TEXT_16,
    val shadow: ButtonShadowInfo? = null,
    val height: DpPxGetter,
    val paddingHorizontal: DpPxGetter = height,
    val borderInfo: BorderInfo? = null,
    val textColor: ColorGetter,
    val underline: Boolean = false,
    val backgroundColor: ColorGetter,
    val invisibleAlpha: Float = 0.25f
) {

    companion object {

        val SMALL_PRIMARY_BORDER = HButtonInfo(
            shadow = ColorManager.DEFAULT_BUTTON_SHADOW_INFO,
            height = dp40,
            textColor = ColorManager.PRIMARY,
            borderInfo = createBorderInfo(ColorManager.PRIMARY),
            backgroundColor = ColorManager.WHITE
        )

        val SMALL_PRIMARY_BACKGROUND = HButtonInfo(
            shadow = ColorManager.DEFAULT_BUTTON_SHADOW_INFO,
            height = dp40,
            textColor = ColorManager.BG,
            backgroundColor = ColorManager.PRIMARY
        )

        private fun createBorderInfo(color: ColorGetter) =
            BorderInfo(
                color = color,
                alpha = 1f,
                width = dp2
            )

    }

    val rippleDrawInfo = RippleDrawInfo(
        rippleInfo = ColorManager.RIPPLE_INFO,
        backgroundColor = backgroundColor,
        color = textColor
    )

}