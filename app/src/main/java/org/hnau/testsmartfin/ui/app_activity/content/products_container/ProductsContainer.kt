package org.hnau.testsmartfin.ui.app_activity.content.products_container

import android.content.Context
import android.view.View
import org.hnau.testsmartfin.data.ProductsManager
import org.hnau.testsmartfin.data.entity.Product
import org.hnau.testsmartfin.ui.utils.ColorManager
import org.hnau.testsmartfin.ui.app_activity.content.products_container.products_pager.ProductsPager
import ru.hnau.androidutils.ui.utils.Side
import ru.hnau.androidutils.ui.view.view_changer.ViewChangerInfo
import ru.hnau.androidutils.ui.view.waiter.loader.SuspendLoader
import ru.hnau.jutils.getter.base.GetterAsync
import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.producer.StateProducerSimple
import ru.hnau.jutils.producer.locked_producer.LockedProducer


class ProductsContainer(
    context: Context,
    onProductClick: (Product) -> Unit
) : SuspendLoader<List<Product>>(
    context = context,
    producer = ProductsManager as Producer<GetterAsync<Unit, List<Product>>>,
    viewChangerInfo = ViewChangerInfo(
        fromSide = Side.BOTTOM,
        scrollFactor = 0.5f
    )
) {

    private val productsProducer = StateProducerSimple<List<Product>>()

    private val pager: ProductsPager by lazy {
        ProductsPager(
            context = context,
            onProductClick = onProductClick,
            productsProducer = productsProducer
        )
    }

    override fun generateContentView(data: List<Product>): View {
        productsProducer.updateState(data)
        return pager
    }

    override fun generateWaiterView(lockedProducer: LockedProducer) =
        ColorManager.createWaiterView(context, lockedProducer)

}