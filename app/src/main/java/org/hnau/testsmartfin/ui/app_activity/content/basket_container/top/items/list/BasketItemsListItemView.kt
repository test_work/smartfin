package org.hnau.testsmartfin.ui.app_activity.content.basket_container.top.items.list

import android.content.Context
import android.widget.LinearLayout
import org.hnau.testsmartfin.data.entity.BasketItem
import org.hnau.testsmartfin.ui.utils.ColorManager
import org.hnau.testsmartfin.ui.utils.SizeManager
import org.hnau.testsmartfin.utils.Amount
import org.hnau.testsmartfin.utils.Weight
import ru.hnau.androidutils.context_getters.toGetter
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.label.Label
import ru.hnau.androidutils.ui.view.list.base.BaseListViewWrapper
import ru.hnau.androidutils.ui.view.utils.apply.*
import ru.hnau.androidutils.ui.view.utils.apply.layout_params.applyLinearParams


class BasketItemsListItemView(
    context: Context
) : LinearLayout(
    context
), BaseListViewWrapper<BasketItem> {

    override val view = this

    private val nameView = createLabel()
        .applyLinearParams {
            setStretchedWidth()
            setEndMargin(SizeManager.DEFAULT_SEPARATION)
        }

    private val totalAmountView = createLabel()
        .applyLinearParams()

    private val topContainer = LinearLayout(context)
        .applyHorizontalOrientation()
        .addChild(nameView)
        .addChild(totalAmountView)

    private val comment1View = createLabel()
        .applyLinearParams {
            setStretchedWidth()
            setEndMargin(SizeManager.DEFAULT_SEPARATION)
        }

    private val comment2View = createLabel()
        .applyLinearParams()

    private val bottomContainer = LinearLayout(context)
        .applyHorizontalOrientation()
        .applyTopPadding(SizeManager.DEFAULT_SEPARATION)
        .addChild(comment1View)
        .addChild(comment2View)

    init {
        applyVerticalOrientation()
        applyPadding(SizeManager.DEFAULT_SEPARATION)

        addChild(topContainer)
        addChild(bottomContainer)
    }

    override fun setContent(content: BasketItem, position: Int) {
        nameView.text = content.product.name.toGetter()
        totalAmountView.text = content.amount.toUiString(true)
        comment1View.text = combineWeightAndAmount(Weight.byKGs(1), content.product.priceByKg, false)
        comment2View.text = combineWeightAndAmount(content.weight, content.product.priceByKg)
    }

    private fun combineWeightAndAmount(
        weight: Weight,
        amount: Amount,
        forceGramms: Boolean = true
    ) =
        "${weight.toUiString(forceGramms).get(context)} x ${amount.toUiString(true).get(context)}".toGetter()

    private fun createLabel() =
        Label(
            context = context,
            textSize = SizeManager.TEXT_16,
            gravity = HGravity.START_CENTER_VERTICAL,
            textColor = ColorManager.PRIMARY_T75,
            maxLines = 1,
            minLines = 1
        )

}