package org.hnau.testsmartfin.data

import org.hnau.testsmartfin.data.db.ProductsDbCacheLevel
import org.hnau.testsmartfin.data.entity.Product
import ru.hnau.jutils.getter.SuspendGetter
import ru.hnau.jutils.producer.CachingProducer


object ProductsManager : CachingProducer<SuspendGetter<Unit, List<Product>>>() {

    override fun getNewValue() =
        SuspendGetter.simple {
            ProductsDbCacheLevel
                .load()
                .sortedBy { it.name }
        }

}