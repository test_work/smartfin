package org.hnau.testsmartfin.ui.app_activity.content.count_selector.input.keyboard

import android.content.Context
import android.widget.LinearLayout
import org.hnau.testsmartfin.R
import org.hnau.testsmartfin.ui.utils.SizeManager
import org.hnau.testsmartfin.ui.app_activity.content.count_selector.input.keyboard.button.CountSelectorButton
import org.hnau.testsmartfin.ui.app_activity.content.count_selector.input.keyboard.button.symbol.NewSymbolCountSelectorButton
import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.ui.view.utils.apply.*
import ru.hnau.jutils.handle
import ru.hnau.jutils.producer.Producer


class CountSelectorKeyboard(
    context: Context,
    value: Producer<String>,
    valueIsCorrectChecker: (String) -> Boolean,
    valueIsChanged: (String) -> Unit
) : LinearLayout(
    context
) {

    companion object {

        private const val COLUMNS = 3

        const val DOT = '.'
        const val EMPTY_PLACEHOLDER = "-"

    }

    private val buttons = run {

        val addButons =
            "123456789${DOT}0".map { symbol ->
                NewSymbolCountSelectorButton(
                    context = context,
                    symbol = symbol.toString(),
                    value = value,
                    valueIsChanged = valueIsChanged,
                    valueIsCorrectChecker = valueIsCorrectChecker
                )
            }

        val backspaceButton =
            CountSelectorButton(
                context = context,
                value = value,
                valueIsChanged = valueIsChanged,
                valueIsCorrectChecker = valueIsCorrectChecker,
                icon = DrawableGetter(R.drawable.ic_backspace),
                valueOnClickConverter = { value ->
                    value.isEmpty().handle(
                        forTrue = EMPTY_PLACEHOLDER,
                        forFalse = value.dropLast(1)
                    )
                }
            )

        return@run addButons + backspaceButton
    }.apply {
        forEach { button ->
            button.applyPadding(SizeManager.EXTRA_SMALL_SEPARATION)
        }
    }

    init {
        applyVerticalOrientation()

        lateinit var rowView: LinearLayout
        buttons.forEachIndexed { number, button ->
            if (number % COLUMNS == 0) {
                rowView = LinearLayout(context)
                    .applyHorizontalOrientation()
                    .applyCenterGravity()
                addView(rowView)
            }
            rowView.addView(button)
        }
    }


}