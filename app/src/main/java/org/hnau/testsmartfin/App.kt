package org.hnau.testsmartfin

import android.app.Application
import ru.hnau.androidutils.utils.ContextConnector


class App : Application() {

    override fun onCreate() {
        super.onCreate()
        ContextConnector.init(this)
    }

}