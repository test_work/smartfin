package org.hnau.testsmartfin.ui.app_activity.content.products_container.products_pager

import android.content.Context
import android.graphics.Color
import android.support.design.widget.TabLayout
import android.widget.LinearLayout
import org.hnau.testsmartfin.data.entity.Product
import org.hnau.testsmartfin.ui.utils.ColorManager
import ru.hnau.androidutils.ui.view.utils.*
import ru.hnau.androidutils.ui.view.utils.apply.applyBackground
import ru.hnau.androidutils.ui.view.utils.apply.applyStartGravity
import ru.hnau.androidutils.ui.view.utils.apply.applyVerticalOrientation
import ru.hnau.androidutils.ui.view.utils.apply.layout_params.applyLinearParams
import ru.hnau.jutils.producer.Producer


class ProductsPager(
    context: Context,
    productsProducer: Producer<List<Product>>,
    onProductClick: (Product) -> Unit
) : LinearLayout(
    context
) {

    private val pager = ProductsViewPager(
        context = context,
        productsProducer = productsProducer,
        onProductClick = onProductClick
    )
        .applyLinearParams {
            setMatchParentWidth()
            setStretchedHeight()
        }

    private val tabLayout = TabLayout(context).apply {
        applyLinearParams { setStartGravity() }
        setupWithViewPager(pager)
        setTabTextColors(
            ColorManager.BG.mapWithAlpha(0.5f).get(context),
            ColorManager.BG.get(context)
        )
        setSelectedTabIndicatorColor(ColorManager.ADDITIONAL.get(context))
        setSelectedTabIndicatorHeight(dpToPxInt(4))
        setTabRippleColorResource(android.R.color.white)
    }

    init {
        applyVerticalOrientation()
        applyBackground(ColorManager.PRIMARY)
        addView(tabLayout)
        addView(pager)
    }

}